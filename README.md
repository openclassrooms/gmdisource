# GMDI source

Google Material Design Icons Source contient les fichiers SVG utilisés pour créer la lib [GMDI sur Draw.io](https://github.com/mikaelflora/gmdi).  

Elle a été conçu à partir des fichiers SVG 24x24 de [GMDI](https://github.com/google/material-design-icons).  

## Contexte

Dans le cadre de la formation de Développeur d'Applications Android j'ai prototypé des applications mobiles avec [Draw.io](https://www.youtube.com/channel/UCiTtRN9b8P4CoSfpkfgEJHA). Je n'ai pas trouvé de libs d'icônes qui me convenait j'ai donc porté les GMDI sur Draw.io.

## Réalisation

J'ai gardé les catégories utilisés par Google et je me suis basé uniquement sur les fichiers SVG de taille 24x24 pixels. Le portage a consisté a :  

  - "re-travailler" les SVG (renommer les SVG, inclure la coloration via CSS)
  - activer les CSS sous Draw.io
  - créer les libs
  - créer les liens de récupération des libs

### Re-travailler les SVG

```bash
# rename SVG files
cd /path/to/category/
for svg in *; do mv $svg ${svg/ic_}; done
for svg in *; do mv $svg ${svg//_24px}; done
# include CSS
for svg in *; do sed -i '2i\    <style type="text/css">\n        .shape {fill:#000000;}\n    </style>' $svg; done
```

Puis édition manuelle des SVG pour rajouter la classe `shape` dans les balises adéquates.  

Par exemple pour `warning` on passe d'un fichier `ic_warning_24px.svg` :  

```svg
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"/>
</svg>
```

A un fichier `warning.svg` :  

```svg
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	<style type="text/css">
		.shape {fill:#000000;}
	</style>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path class="shape" d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"/>
</svg>
```

### Activer les CSS sous Draw.io

Pour activer les propriétés CSS sous Draw.io, il faut modifier le style `[Ctrl] + [E]` du SVG afin de rajouter l'instruction suivante :  

```
editableCssRules=\.*;
```

A partir de maintenant la couleur du SVG peut être modifiée dans Draw.io.  

### Créer les libs

Créer la lib en allant dans `Fichier > Nouvelle librairie > GitHub...`. Se connecter a son compte GitHub et créer le dépôt qui va héberger la lib.  

Ajouter chaque SVG dans la lib en leur associant un nom (ici nous récupérons les noms des SVG).  

### Créer les liens de récupération des libs

Pour chaque lib nous récupérons l'URL `Raw` sur GitHub. On applique un URL Encode sur celles-ci (jGraph propose [son outil](https://jgraph.github.io/drawio-tools/tools/convert.html) pour réaliser l'URL encode). Ensuite nous rajoutons comme préfix la racine `https://www.draw.io/?splash=0&clibs=U`. Chaque URL de lib est séparée par `;U`. Ce qui donne par exemple :  

```
# une lib
https://www.draw.io/?splash=0&clibs=Uhttps%3A%2F%2Fraw.githubusercontent.com%2Fmikaelflora%2Fgmdi%2Fmaster%2FGMDI%2FGMDI-Action.xml
# deux libs
https://www.draw.io/?splash=0&clibs=Uhttps%3A%2F%2Fraw.githubusercontent.com%2Fmikaelflora%2Fgmdi%2Fmaster%2FGMDI%2FGMDI-Action.xml;Uhttps%3A%2F%2Fraw.githubusercontent.com%2Fmikaelflora%2Fgmdi%2Fmaster%2FGMDI%2FGMDI-Alert.xml
# etc. ...
```

## Conclusion

Cette activité m'a permis de découvrir la création de lib Draw.io et d'obtenir une lib utile.  

Voici [la lib GMDI pour Draw.io](https://github.com/mikaelflora/gmdi).  

## Sources

[Draw.io](https://www.draw.io)  
[Create and share custom libraries](https://github.com/jgraph/drawio-libs)  
[Material Design Icons - Google repository](https://github.com/google/material-design-icons)  

